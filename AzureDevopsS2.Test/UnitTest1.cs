using NUnit.Framework;
using AzureDevopsS2.Web.Operations;

namespace AzureDevopsS2.Test
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestCase(3, 4, 7)]
        public void Test1(int a, int b, int expectedResult)
        {
            HomeBusiness hb = new HomeBusiness();
            Assert.AreEqual(expectedResult, hb.Sum(a, b));
        }
    }
}